set(classes
  vtkPolygonArcOperation
  vtkPolygonArcProvider
  vtkPolygonContourOperation)

vtk_module_add_module(vtkPolygonOperationsExt
  CLASSES ${classes})
# Link explicitly to vtkSMTKOperationsExt, which can't be set in module.cmake
# because this is invoked by separate vtk_smtk_process_modules in polygon
# session
vtk_module_link(vtkPolygonOperationsExt
  PUBLIC
    smtkCore)

# The list of public headers for a VTK module are stored as a "module property"
# (which is just a namespaced CMake TARGET property with some extra support for
# being different at install time). Ask for the headers and replace any
# in-source absolute paths with relative paths.
_vtk_module_get_module_property(vtkPolygonOperationsExt
  PROPERTY headers
  VARIABLE vtk_headers)
string(REPLACE "${CMAKE_CURRENT_SOURCE_DIR}/" "" vtk_headers "${vtk_headers}")
smtk_public_headers(vtkPolygonOperationsExt ${vtk_headers})
